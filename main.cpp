#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <sstream>

using namespace std;

static vector<vector<int> > results;

vector<vector<int> > comboBreaker( vector<vector<int> > & costPerCountryToWin, vector<vector<int> > & votesNeededPerPos, int pos, const int diff, int cost, int points, vector<int> & k)
{
    if (pos<costPerCountryToWin.size()){
        for(int i=0;i<costPerCountryToWin[pos].size();i++){
        }
    }
}

int main()
{
    fstream myfile;
    string x;
    myfile.open ("pliki/eur00.in", std::fstream::in | std::fstream::out | std::fstream::app);

    // The result of the read is placed in here
    // In C++ we use vector as it is like an array but can dynamically grow
    // as required when we get more data.
    std::vector<std::vector<int> >     data;

    // Replace 'Plop' with your file name.

    std::string   line;
    // Read one line at a time into the variable line:
    while(std::getline(myfile, line))
    {
        std::vector<int>   lineData;
        std::stringstream  lineStream(line);

        int value;
        // Read an integer at a time from the line
        while(lineStream >> value)
        {
            // Add the integers from a line to a 1D array (vector)
            lineData.push_back(value);
        }
        // When all the integers have been read add the 1D array
        // into a 2D array (as one line in the 2D array)
        data.push_back(lineData);
    }

    for(int i=0;i<data.size();i++){
        for(int j=0;j<data[i].size();j++){
            //cout << data[i][j] << "\t";
        }
        //cout << endl;
    }

    //cout << endl;

    //program start

    int n = data[0][0];
    int m = data[0][1];
    int k = data[0][2];

    vector<int> cost;
    for(int i=0;i<m;i++){
        cost.push_back(data[1][i]);
        //cout<<"cost"<<cost[i]<<endl;
    }

    vector<int> pointsPerPos;
    for(int i=0;i<k;i++){
        pointsPerPos.push_back(data[2][i]);
        //cout<<"pojits"<<pointsPerPos[i]<<endl;
    }

    vector<int> pointsForCountry(n,0);

    for(int j=0;j<m;j++){
        vector<int> votes = data[j+3];
        sort(votes.begin(),votes.end());
        reverse(votes.begin(),votes.end());
        votes.erase( unique( votes.begin(), votes.end() ), votes.end() );
        for(int i=0;i<n;i++){
            for(int x=0;x<n;x++){
                if(data[j+3][x]==votes[i] && i<pointsPerPos.size()){
                    pointsForCountry[x] += pointsPerPos[i];
                }
            }
        }
    }

    //cout<<"---------------------"<<endl;
    int max=0;
    for(int i=0;i<n;i++){
        //cout << pointsForCountry[i]<<endl;
        if(max<pointsForCountry[i])
            max = pointsForCountry[i];
    }

    static int diff;
    if (max<pointsForCountry[0])
        return 0;
    else
        diff = max-pointsForCountry[0];

    //cout << diff <<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl;

    //------------------------------------------

    vector<vector<int> > votesNeededPerPos;

    for(int j=0;j<m;j++){
        vector<int> votesNeeded(k,0);

        vector<int> votes = data[j+3];
        sort(votes.begin(),votes.end());
        reverse(votes.begin(),votes.end());
        votes.erase( unique( votes.begin(), votes.end() ), votes.end() );

        for(int i=0;i<k;i++){
            votesNeeded[i] = votes[i] - data[j+3][0];
        }
        votesNeededPerPos.push_back(votesNeeded);
    }

    for (int i=0;i<votesNeededPerPos.size();i++){
        for (int j=0;j<votesNeededPerPos[i].size();j++){
            //cout<<votesNeededPerPos[i][j]<<"\t";
        }
        //cout<<endl;
    }

    //cout << diff <<endl<<endl<<endl<<endl<<endl;


    vector<vector<int> > costPerCountryToWin = votesNeededPerPos;
    for (int i=0;i<costPerCountryToWin.size();i++){
        for (int j=0;j<costPerCountryToWin[i].size();j++){
            //cout<<"info"<<votesNeededPerPos[i][j]<<"\t"<<cost[j]<<"\t"<<endl;
            costPerCountryToWin[i][j] = votesNeededPerPos[i][j]*cost[i];
        }
    }

    for (int i=0;i<costPerCountryToWin.size();i++){
        for (int j=0;j<costPerCountryToWin[i].size();j++){
            //cout<<costPerCountryToWin[i][j]<<"\t";
        }
        //cout<<endl;
    }

    vector<int> totalCost(pow(k+1,costPerCountryToWin.size()+1),0);
    vector<int> totalPoints(pow(k+1,costPerCountryToWin.size()+1),0);

    //cout<<totalCost.size()<<endl;

    vector<int> poorMansBase3(m,0);

    int min=pow(2,30);

    vector<int> solution;

    for(int j=0;j<totalCost.size();j++){
        for(int i=1;i<m;i++){
            if(poorMansBase3[i]==k) ;
            else {
                totalCost[j]+=costPerCountryToWin[i][poorMansBase3[i]];
                totalPoints[j]+=pointsPerPos[poorMansBase3[i]];
                //cout<<"\t "<<costPerCountryToWin[i][poorMansBase3[i]]<<"\tpojits:\t"<<pointsPerPos[poorMansBase3[i]]<<endl;
            }
        }
        ////cout<<endl;
        ////cout<<totalCost[j]<<endl;
        ////cout<<totalPoints[j]<<endl;
        ////cout<<"points: \t"<<totalPoints[j]<<"\t total cost: \t"<<totalCost[j]<<endl;
        if((totalCost[j]<min) && (totalPoints[j]>diff)){
            solution = poorMansBase3;
            min=totalCost[j];
            ////cout<<"\t newmin:\t"<<totalCost[j]<<"\tpojits:\t"<<totalPoints[j]<<endl;
        }//else //cout<<"\t "<<totalCost[j]<<"\tpojits:\t"<<totalPoints[j]<<endl;
        for(int i=0;i<poorMansBase3.size();i++){
            //if(i!=0){
                int sometin = costPerCountryToWin.size()-1;
                int temp = floor(j/pow(k+1,sometin-i));
                ////cout<<j;
                ////cout<<temp<<"\t";
                poorMansBase3[i] = temp % (k+1);
            //}
            //cout<<poorMansBase3[i];
        }
    }
    cout<<endl<<min<<endl;
    for(int i=0;i<m;i++){
        cout<<solution[i];
    }
    ofstream newfile("eur01.out");
    newfile << min << "\n";
    for(int i=0;i<solution.size();i++){
        if(solution[i]!=k){
            newfile << votesNeededPerPos[i][solution[i]] << " ";
        } else {
            newfile << 0 << " ";
        }
        for(int j=1;j<(n-1);j++){
            newfile << 0 << " ";
        }
        newfile<<0<<"\n";
    }
    newfile.close();


    return 0;
}
